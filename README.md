# st

*simple terminal*

--------------------

st is a simple terminal emulator for X which sucks less.

This is my own heavily patched distribution of st. As of now I have applied the following patches:

- alpha
- anysize
- appsync
- bold-is-not-bright
- boxdraw
- desktopentry
- dynamic-cursor-color
- fix-keyboard-input
- font2
- glyph-wide-support-boxdraw
- gruvbox-material
- newterm-0.9
- scrollback-ringbuffer
- scrollback-mouse
- scrollback-mouse-altscreen
- vertcenter
- workingdir
- xclearwin
- [This wide glyph fix](https://github.com/LukeSmithxyz/st/pull/349#issuecomment-1374333271)



